## Security

* Never give outside access to projects or groups in this heirarchy.
* All groups need to disable "Allow users to request access (if visibility is public or internal)"
* Never use tokens from your personal gitlab account on projects anywhere in guided-explorations
* There is a special user id associated with this set of repositories and an associated personal access token.
    * Consult the SA vault to learn the user and token
    * Do not hard code the username, use a CI / CD variable
    * Add the token and/or user as variables DIRECTLY to the CI / CD variables on the **PROJECT** you are enabling - not in any group anywhere in the heirarchy.
    * MASK BOTH variables

## Conventions
These conventions are intended to help prevent duplicate structures within Guided Explorations


* Guided Explorations using "Custom User Groups" use one of the existing user group heirarchies under https://gitlab.com/guided-explorations/user-groups
* Guided Explorations involving CI / CD Plugin Extensions are housed in: https://gitlab.com/guided-explorations/templates/ci-cd-plugin-extensions
* Guided Explorations requiring an access token in CI, use the standard user: TBD

## Tags
* **Defacto Functionality In Many Completing Products** - Provides working, real world examples / demos / learning activities for doing common DevOps activites that are explicitly enabled in similar DevOps products.
* **Enable New Feature Adoption** - Provides working, real-world examples / demos / learning activities for recently released features (a common ask in PreSales).
* **Prepare For Likely SA Asks** - Provides working, real-world examples / demos / learning activities for likely technical asks during Presales meetings / followups / etc.
* **Channel Enablement** - Channel partners can use the content to do one or more of: Train in product internally, Give sales demos, Create GitLab Plugin Integrations
* **Competitive Play - PRODUCTNAME** - Provides working, real world examples / demos / learning activities to compete with feature / function in competing products.

## Plugin Extensions Conventions

1. Always include the following comment emitted in logs (and therefore also visible in source): 

    `echo "CRITICAL ERROR: This original location for this plugin extension (http://$CI_SERVER_HOST/$CI_PROJECT_PATH) is for demonstration and learning purposes only.  There is no SLA for breaking changes and it may change at any time without notice. If you need to depend on it, please make your own isolated copy to depend on. (and delete this message from your copy)"`
