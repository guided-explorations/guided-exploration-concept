> Important: Security is discussed in [CONTRIBUTING.md](CONTRIBUTING.md)

Contributors: @DarwinJS, @cnwachukwu

## The "Guided Explorations" Concept

The document "[Guided Explorations - Designed by Questions](./guided-explorations-designed-by-questions.md)" details how this concept was formed by answering the following questions:

1. **Why is it hard to understand whether GitLab can be implemented to serve a specific desired developer workflow?**
2. **For which personas is this key learning important for and when in their journey is it critical?**
3. **Could one solution design serve the needs of multiple persona scenarios?**
4. **Why can presales be an especially challenging time frame for this problem for GitLab customers and presales technical support?**

## Major Philosophies Reflected

### Reduction to Practice (Prototyping)
Guided Explorations encourage that individuals and groups to drive "theories of what might be done" to working examples (referred to US Patent law as ["Reduction to Practice"](https://en.wikipedia.org/wiki/Reduction_to_practice)).  This has good overlap with Solution Architecture as the solution is generally required to be production viable in a tight timeframe around a single parties need to unblock on a well defined problem.

### Evergreen Content
Evergreen content is the approach of creating something that does not age and therefore retains relevance to it's audience.  Guided Explorations through strong encouragement to identify an owner/steward and to setup continuous testing and fixing of solutions that are placed here.

### Seed Bank (Derivitive Solution Incubation)
Yep, just repurposed that term right before your eyes ;) Guided Explorations should result in propagation of sophisticated solutions that are based on them as a starting point.  By being explicit in what they demonstrate and in trying to be useful learning aides **and** good starting points for derivative work.

## Modes Of Use

> Important: "Guided Explorations" have a meta-scope larger than "a repository project" - they can show concepts and structures that involve multiple projects and groups.

- **Demos** - both prebaked results demos and live change demos by GitLab, Customers or Partners.
- **Templates** - starting points for:
    * Prototypes
    * Proof of Concept (POC) / Proof of Value (POV) or Minimum Viable Product (MVP)
    * Real projects
- **Learning Activities and Tracking** - when enhanced by "Exercises" using markdown issue templates with checkboxes - can function as a team level LMS with tracking.

![compound-reuse-of-guided-explortions](./compound-reuse-of-guided-explortions.png)

### Guided Exploration Characteristics

Many of the highlighted characteristics serve to differentiate "Guided Explorations" from "Once and Done Open Sourced Example Snippets".

#### Simplicity

Only requires GitLab functionality to work. However, can also be wrapped for specific purposes - such as in a Learning Management System (LMS).

#### Actively Managed, Not a Graveyard

Guided Explorations are intended to be **Product Managed** actively into the future, mindful of usability, bug fixed, updated for new GitLab features that supersede workarounds in the Guided Exploration. As mentioned later, they should also have nightly builds as a form of automated testing against changes in the GitLab version they run on.

Contributors who onboard an entire Guided Exploration need to identify themselves as the product manager in the README.md.

#### High Level Characteristics
- **Is Directly Reusable for Enablement**: both interally at GitLab and externally by customers, for the activities of pre-sales technical qualification, demonstration of features to stakeholders, onboarding of team members to GitLab features (as applied to defacto development patterns), templates for starting points for real-world projects of the given pattern.
- **Maximizes Reusability**: Guided Explorations seek to take the cornerstone concept of <u>resuability of code</u> and apply it to enablements.
- **Is a Working Example Pattern**: forcing a build results in a successful build - because "Working Examples" = "The Automation of Understanding Complex Systems"
- **Leverages and Limits Itself to Built-In GitLab Features** **(Everyone Can Contribute)**: to support the GitLab vision "Anyone Can Contribute".  By avoiding tethering to specialized learning systems, GitLab team members and customers are fully enabled to build and share Guided Explorations with their existing GitLab and coding skills.  Everyone is "**Enabled to Build Enablements**". (Enablements Squared?)
- **Full Defacto (Real World) Development Patterns**: can be accomodated and are the preferred focus of Guided Explorations.  This adds more value than the typical stumbling around required to assembly many example snippets into a working, life-like pattern.
- **Is Directly Usable for Pre-Baked Demos**: where the existing build logs and output can be directly demoed to show how the pattern works.
- **Can Have a Broader Pattern Scope Than "A Single Repository":** to demonstrate complex concepts such as group level inheritance, multi-repository concepts and instance and group level roll up features like the various dashboards.
- **Contains Verbose Teaching Text**: in the form of documents, exercises, code comments, etc.  Teaching text should assume an audience with minimal experience so that it is maximally reusable.
- **Contains Duplication Instructions**: This means the Guided Exploration includes setup documentation that enables anyone to duplicate the Guided Exploration in a GitLab instance or group where they have full ownership of all aspects.
- **Can Contain <u>Observation Exercises / Demo Guides</u> (Highly Encouraged):** that allow learners to understand concepts by observing a working model. If the observations require higher permissions levels, the Guided Exploration may still need to be duplicated to a location where learners can be giving a higher permission level. The same instructions should be usable for both Observation and Demos - the difference being whether the audience is the single individual performing the steps for the first time, or an experienced individual showing the steps to an audience.
- **Can Contain <u>Interaction Exercises / Demo Guides</u>**: that instruct learners on how to make changes, run builds and perform other actions that cause the working example to actually work before their eyes. Having one or more of these is what makes the Guided Exploration much more helpful for onboarding team members to GitLab. The same instructions should be usable for both Observation and Demos - the difference being whether the audience is the single individual performing the steps for the first time, or an experienced individual showing the steps to an audience.
- **Can Be Backed By or Proceeded By Blogs / Videos (Optional)** : Guided Explorations can be built and then promoted through how to blogging or how to videos.  A Guided Exploration can also be built to support an existing or planned how to blog or video. When this is done, the Guided Exploration README.md should cross reference the Blog / Video and vice versa.
- **Is Strongly Opinated to a Tool / Template Disposition**: working examples can require many manual updates to hard coded data in order to be leveraged for another similar scenario. Guided Explorations strongly embrace soft coding for all input data, verbose commenting and articulate naming in code (job names, variable names clearly indicate their purpose) and how to documentation to maximize direct reuse with minimal adaptation. Obvioulsy this helps with the reuse of Guided Explorations across multiple GitLab customers, but more importantly, many customers have internal enablement and/or developer tooling teams who wish to build and directly promote standard best practice by encouraging or enforcing use of centally managed templates.  When Guided Explorations have a strong template orientation they are maximize direct reusability in these scenarios.
- **Explicitly Documents GitLab Features and Editions**: required to successfully use the Guided Exploration. This helps everyone understand both the value of GitLab and licensing requirements for GitLab to implement the demonstrated pattern. 
    This approach also enabless self-assessment of whether a demonstrated pattern retains it's value to a specific development scenario when some required features are not in the customer's planned or current product edition.  This enables the subsequent assessment of work arounds if a few Guided Exploration features have low value to the customer scenario.  It also enables the subsequent assessment of the total value of higher product licenses if there is a critical mass of features in that edition across one or more Guided Explorations.

#### Specific Settings and Requirements

- See [CONTRIBUTING.md](CONTRIBUTING.md) for information on development conventions to help prevent duplicate mechanisms from being added to shared Guided Exploration mechanisms (e.g. User Groups, API User)
- Lists the major patterns demonstrated in the repository "Description" field (because data here responds to repository "name" searches)
- Contains old merge requests, issues, build logs that can be used directly in “pre-baked results” demos.
- Has a scheduled nightly build for always on QA test and optionally to assure fresh "pre-baked results" for demoing.
- For Interaction Exercises that need a known "broken" or "undesirable" state, the exploration shuld implement these as special "starting point" branches.  This allows the mainline branches to always work for observation exercises, automated QA and prebaked demos.
- README.md with specific standard sections explaining the repo's usefulness as a Guided Exploration
    1. Overall GitLab Edition Needed for Features (Free / Bronze / Silver / Gold)
    
    2. List of GitLab features demonstrated **with indication of GitLab Edition for each** (Free / Bronze / Silver / Gold)
    
    3. Edition Images (more in images subfolder): ![FC](images/FC.png),![SB](images/SB.png),![PS](images/PS.png),![UG](images/UG.png)
- **.gitlab/issue_templates/01-Setup-Repository.md** - formatted as a markdown checklist, contains minimal instructions to import the repo and get everything setup so that builds will work in the new home location (on any instance anywhere). Including what the repo conceptually demonstrates and what specific apis it calls (e.g. .gitlab-ci.yml statements).  In addition, if it demostrates anything that is “novel” or non-obvious.

#### The Guided Part (optional, but encouraged)
Even if a repository simply adhere to the points above, it becomes **much, much more useful** than a non-working snippet.
  - one or more .gitlab/issue_templates/0X-Guided-Exploration-Lorum-Ipsum.md for users to simply make observations about the functionality of the repository with it's existing job history and configuration.  Can be done to a copy of the repo the user cannot change.
  - one or more .gitlab/issue_templates/0X-Guided-Exploration-Lorum-Ipsum.md for users to make CHANGEs to the repo and observe results - formatted as a structured checklist.  User may need to copy and setup to repo to do these types of exercises.

## Uses (Compounded Reuse)

### Customer Self-Enablement Presales:

If done right, this may be able to reduce the hands-on touches to a customer before they buy since they will be enabled to discover and self-trial and even do internal self-demos of significant features.  Enable customers to sell themselves.

- Product evaluator / evaluation team self-exploration
- Product evaluator / evaluation team self-demo to influence internal stakeholders
- Used for: post-webinar DIY reference

### Customer Self-Enablement Post Sales:

- GitLab product implementation self-training (with per-individual tracking)
- New customer SA team member self-training long after implementing  (with Per-individual tracking)
- Post sales call DIY reference
- Marketing video or blog DIY reference to concepts covered (created in concert with Video/Blog or vice versa)
- Import one or more to a custom repo template library

### GitLab Internal Enablement: 

For a) newly released features education and b) newly minted team member onboarding

- Product teams providing immediately usable, demonstratable new feature explorations by generating a Guided Exploration as part of feature release
- Onboarding new support / development . sales team members self-training (with per-individual tracking)
- SAs use format to build-out new demos - especially if likely to be a golden repo candidate
- Sales setting up new demos
- Finding existing demos

### Channel Enablement (Products and/or Services): 

- Products Channel: Understanding how to integrate with GitLab (e.g. Creating Custom, Templatable Plugin Extensions)
- Products & Services Channels: Onboarding new support / development . sales team members self-training (with per-individual tracking)
- Products & Services Channels: Demonstrating GitLab
- Professional Services Channels: Sales demos and consultant onboarding
- Alliances Channels: With [Examples of how to build CI CD Plugins](https://gitlab.com/guided-explorations/templates/ci-cd-plugin-extensions)

## Audible Ready Subject Matter Expertise (SME) Development
* Building out a Guided Exploration frequently pushes the builder's knowledge of the product out to "Audible Ready Subject Matter Expert" levels.

## Early / Deep Adopter Product Feedback
* Building out Guided Explorations frequently identifies bugs and defacto expectation gaps in product (aka early adopter feedback) (e.g. lack of .NET Core 3 support)
* Special Guided Explorations for features behind feature switches.

### Index of Patterns
[Index of Patterns](./index-of-patterns.md)
