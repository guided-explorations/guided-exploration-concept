## What questions does the Guided Exploration Solution seek to answer?

1. **Why is it hard to understand whether GitLab can be implemented to serve a specific desired developer workflow?**
   - Development workflows are complex combinations of customer specific processes, development culture & behaviors and technical limitations and enablements (all of which may have some flexibility to accommodate available capabilities) - they are challenging to create even when full knowledge of available tooling is possessed by workflow engineers (Agile\SCM\CI\CD Engineering).
   - GitLab is a rich set of features and primitives (small units of functionality) that are composed into a workflow subassembly or solution - it is hard to know all the small features, their possible creative combinations and possible workarounds that need to be assembled to build a given desired workflow.  
     For instance - GitLab has a primitive known as a "CI/CD Variable" which is a simple idea. However after adding group CI/CD variables that are inherited (another primitive) and CI/CD environment scopes (another primitive) the resultant compounded functionality is sufficent for many sophisticated implementations that can satify many patterns required by customers - but these primitives haven't necessarily been designed around any given one of these customer needs. 
   - It is a natural human proclivity to assume All You Can See Is All There Is (AYCSIATI) - including when individuals are evaluating a product choice. So when a given set of requirements do not have a notable expression in the GitLab user experience, production adoption evaluators can tend to assume the capability does not exist. This is made worse when competing products do have a user experience around the feature set as it becomes even more expected to see a similar user experience in GitLab.
   - GitLabs continuous release process compounds this challenge because more specific functionality is constantly being added to address scenarios that previously required devising and building a workflow subassembly or workaround.
   - Coding (including GiLab workflow coding) is best learned through working examples where the coding and it's successful results can be observed as it teaches concepts, design, implementation and building blocks all at once in a validated pattern (because working results can be observed). "Working Examples" = The Automation of Understanding.
2. **For which personas is this key learning important for and when in their journey is it critical?**
   - Buyers / Evaluation Teams - during the presales stage.
   - GitLab Team members - during onboarding.
   - GitLab Customer Success team members who are uniquely tasked to be a catalyst to customer understanding.
   - Customer Teams - during initial implementation of GitLab.
   - Customer Team members - during onboarding when working with a productionized GitLab development workflow implementation.
3. **Could one solution design serve the needs of multiple persona scenarios?**
   - Yes, it seems so.
4. **Why can presales be an especially challenging time frame for this problem for GitLab customers and presales technical support?**
   - For customers who have an existing development work flow of intermediate or advanced complexity, they are challenged to know very in-depth things about the product to estimate whether their existing development workflows can be accommodated as precondition to purchasing.
   - While this challenge is the reason why the Solutions Architect role exists and why Trials and POVs are undertaken - it makes sense to attempt to automate this knowledge acquisition as much as possible.

### "Working Examples" = The Automation of Understanding Complex Systems