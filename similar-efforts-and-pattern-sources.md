Many of the examples below are abandon and/or not as reusable as Guided Explorations (it's not easy to figure out how to leverage it for yourself).

### Repositories of Examples
* https://gitlab.com/gitlab-examples
* https://gitlab.com/gitlab-com/customer-success/demos/golden-repos
* Customer maintained SonarQube plugin extension: https://gitlab.talanlabs.com/gabriel-allaigre/sonar-gitlab-plugin#usage
* https://gitlab.com/allthethings
* https://gitlab.com/the_beast

### Issues Tracking Similar Concepts
* https://gitlab.com/gitlab-org/gitlab/issues/10394
* https://gitlab.com/gitlab-org/gitlab/issues/26220
* re-packaged and extensible deploy templates (https://gitlab.com/gitlab-org/gitlab/issues/201487)


### Templates Initiative in Verify Group
Activity: Low
* https://about.gitlab.com/direction/verify/templates/
* https://gitlab.com/groups/gitlab-org/-/epics/2249
* https://gitlab.com/groups/gitlab-org/-/epics/2072
* https://www.youtube.com/watch?v=oZgov4ltl1Q

