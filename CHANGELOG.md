
 ## [0.1.0] - 2020-01-29
### Added
- "Channel Enablement (Products and/or Services)" to [README.md Channel Enablement](README.md#user-content-channel-enablement-products-andor-services)
- "Early / Deep Adopter Product Feedback" to [README.md "Uses"](README.md#user-content-early-deep-adopter-product-feedback)
- "Audible Ready Subject Matter Expertise (SME) Development" to [README.md "Uses"](README.md#user-content-audible-ready-subject-matter-expertise-sme-development)
- "Modes of Use" to [README.md "Modes of Use"](README.md#user-content-modes-of-use)
- "Tags" to [CONTRIBUTING.md "Tags"](CONTRIBUTING.md#user-content-tags)

## [0.0.9] - 2020-01-28
### Added
- [Started Backlog] (https://gitlab.com/guided-explorations/guided-exploration-concept/issues)
- Created some issue tags
- [CHANGELOG.md](CHANGELOG.md)
- [CONTRIBUTING.md](CONTRIBUTING.md)
- [similar-efforts-and-pattern-sources.md](similar-efforts-and-pattern-sources.md)
