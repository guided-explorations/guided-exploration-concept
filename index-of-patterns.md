## Index of Patterns In Guided Explorations <!-- omit in toc -->

## Contents <!-- omit in toc -->

- [<u>Monorepo</u>](#umonorepou)
- [Dedicated User Groups](#dedicated-user-groups)
- [<u>Complex Configuration Data Scenarios</u>](#ucomplex-configuration-data-scenariosu)
- [<u>Branch Per Deployment Environment</u>](#ubranch-per-deployment-environmentu)
- [<u>Re-Usable Configuration File Generation Custom Build Extension / Plugin</u>](#ure-usable-configuration-file-generation-custom-build-extension--pluginu)
- [<u>Building &quot;Custom Build Extension / Plugins&quot; in GitLab</u>](#ubuilding-%22custom-build-extension--plugins%22-in-gitlabu)
- [<u>Make a Custom Extension / Plugin Available as a CI/CD Template Design Pattern</u>](#umake-a-custom-extension--plugin-available-as-a-cicd-template-design-patternu)
  - [File Templates](#file-templates)
  - [Project Templates](#project-templates)

### <u>Monorepo</u>

Leveraging a single repository to build multiple application services or tiers.  Generally used to simplify code collaboration and merge requests for changes that materially affect multiple services at one time.

  **Patterns**

  [Config Data Monorepo](https://gitlab.com/guided-explorations/config-data-top-scope/config-data-subscope/config-data-monorepo)

### Dedicated User Groups

With the appropriate configuration GitLab can be made to create a near equivalent to user groups used in operating systems and other Role Based Access Control (RBAC) systems.

  **Patterns**

  [User Groups](https://gitlab.com/guided-explorations/user-groups/user-groups-readme/blob/master/README.md)


### <u>Complex Configuration Data Scenarios</u>

When deploying the same code base to many environments, configuration data management is paramount. This includes global defaults and per-subscope overrides of defaults and it includes secrets management.  These examples demonstrate most of the ways GitLab can configure multi-layered data with convergence and therefore how GitLab can **compete with the rich configuration data models** of dedicated CD products such as XebiaLabs, Octopus Deply and ArgoCD.

  **Patterns**

  [Config Data Monorepo](https://gitlab.com/guided-explorations/config-data-top-scope/config-data-subscope/config-data-monorepo)

### <u>Branch Per Deployment Environment</u>

GitLab supports multiple patternss to target a deployment environment. These patterns demonstrate utilizing repository branches to target a deployment environment.  This pattern is useful when:

* Application deployment involves per-environment artifacts, such as configuration files.

* When maximum gating controls are desired over environment updates - by isolating environments by branch, deployments can require a merge request - which is where the maximum gating controls exist within GitLab.

* An alterative is to re-trigger the deploy phase with CI/CD variables to target environments - however this makes the last deployment "records" consist of more ephemeral data and the only available gating control is "Protected Environments".

This pattern can be combined with a repository pattern that also reflects deployment design.  For instance, a repository per SaaS customer and within each repositor, a branch per customer environment (when multiple customer environments are supported, such as "UAT" and "Production")

  **Patterns**

  [Config Data Monorepo](https://gitlab.com/guided-explorations/config-data-top-scope/config-data-subscope/config-data-monorepo)

### <u>Re-Usable Configuration File Generation Custom Build Extension / Plugin</u>

The example utilitizes the maven container with Apache FreeMarker to render template configuration files into final config files containing GitLab CI/CD Variables.

- This example uses [Apache FreeMarker](https://freemarker.apache.org) and the [freemarker-cli](https://github.com/sgoeschl/freemarker-cli/blob/master/README.md) project to provide **<u>config file templating capabilities</u>** - similar to Jinja - but Java oriented. It happens to also be the same engine used by the [XebiaLabs](https://xebialabs.com) product.

- When the job is used in a container (here it is using the public maven container) it can be used with any language framework for file templating.  For instance, even if you have a .NET application that needs a bunch of configuration file templating, you can use the job definition in here for your configuration file templating using a free, open source, Apache project standard approach.

  **Patterns**

  [Config Data Monorepo](https://gitlab.com/guided-explorations/config-data-top-scope/config-data-subscope/config-data-monorepo)

### <u>Building &quot;Custom Build Extension / Plugins&quot; in GitLab</u>

These patterns demonstrate how to build a custom extensions/plug-ins and that these Build Extensions / Plugins can be used regardless of the languages being used by the rest of the CI/CD jobs in the same pipeline.

  **Patterns**

  [Config Data Monorepo](https://gitlab.com/guided-explorations/config-data-top-scope/config-data-subscope/config-data-monorepo)

  [CI/CD Plugin Extension Freemarker File Templating](https://gitlab.com/guided-explorations/templates/ci-cd-plugin-extensions/ci-cd-plugin-extension-freemarker-file-templating)

### <u>Make a Custom Extension / Plugin Available as a CI/CD Template Design Pattern</u>

Using GitLabs built-in features for making a custom extension / plugin available as part of new repository templates and new .gitlab-ci.yml files.

#### File Templates

File templates allow a [specific files of the repository can be applied to an existing project](https://docs.gitlab.com/ee/user/admin_area/settings/instance_template_repository.html) (e.g. .gitlab-ci.yml, .Dockerfile, etc)

- For both self-hosted and GitLab.com, you can use the [Group file templates feature](https://gitlab.com/help/user/group/index.md#group-file-templates-premium) ![FC](../config-data-repo-scope/images/PS.png) can be used.
- If you self-host, you can setup an instance-wide custom file template repository through the [Instance Template Repository](https://docs.gitlab.com/ee/user/admin_area/settings/instance_template_repository.html) feature. ![FC](../config-data-repo-scope/images/P.png)

#### Project Templates

Project templates used to create an entire repository.  You specify a group that contains all your repository templates and then when users create new repositories, [they can select from the list of templates](https://docs.gitlab.com/ee/gitlab-basics/create-project.html#custom-project-templates-premium).

- For self-hosted or GitLab.com the [Custom Group-Level Project Templates](https://docs.gitlab.com/ee/user/group/custom_project_templates.html#custom-group-level-project-templates-premium) feature. ![FC](../config-data-repo-scope/images/PS.png)
- If you self-host, you can setup an instance-wide custom project template group through [Custom instance-level project templates](https://docs.gitlab.com/ee/user/admin_area/custom_project_templates.html) feature ![FC](../config-data-repo-scope/images/P.png).

  **Patterns**

  [Config Data Monorepo](https://gitlab.com/guided-explorations/config-data-top-scope/config-data-subscope/config-data-monorepo)